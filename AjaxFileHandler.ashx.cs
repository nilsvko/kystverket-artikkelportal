﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace KYVUPLOAD
{
    /// <summary>
    /// Summary description for AjaxFileHandler
    /// </summary>
    public class AjaxFileHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                try
                {
                    HttpPostedFile file = context.Request.Files[0];
                    //string name = context.Request["name"] != null ? context.Request["name"] : String.Empty;
                    string name = file.FileName;
                    string fileName = String.Empty;

                    string ext = name.Substring(name.LastIndexOf("."));
                    fileName = String.Format("{0}_{1}{2}", MakeValidFileName(name.Remove(name.LastIndexOf("."))), DateTime.Now.ToString("yyMMddHHmmssfff"), ext);
                    //context.Session["upload_file_name"] = fileName;

                    string path = context.Server.MapPath("~/TEMP");
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    string filePathName = Path.Combine(path, fileName);
                    file.SaveAs(filePathName);

                    context.Response.ContentType = "text/plain";
                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    var result = new { name = fileName };
                    context.Response.Write(serializer.Serialize(result));
                }
                catch (Exception)
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("0");
                }
            }
        }

        public static string MakeValidFileName(string name)
        {
            char[] invalid = Path.GetInvalidFileNameChars();
            StringBuilder builder = new StringBuilder();

            foreach (char cur in name)
            {
                builder.Append(invalid.Contains(cur) ? '_' : cur);
            }

            return builder.ToString();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}