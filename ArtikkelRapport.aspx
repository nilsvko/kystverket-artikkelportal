<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArtikkelRapport.aspx.cs" Inherits="Artikler.ArtikkelRapport" %>

<!DOCTYPE html>
<script runat="server">

    protected void ArtiklerGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8">
    <title>KARTLEGGING - STATLIGE FISKERIHAVNER</title>
    <style>
  
    body {
        background:#fff;
        color:#000;
        font-family:Helvetica, sans-serif;
        font-size: smaller;                  /* Setter font til X-small for å få plass til alt på en side */
    }

    .header {
        background:#99CDFF;
        width:100%;
    }

    .header-text {
        font-size:16pt;
        padding-top:10px;
    }

    .logo {
        padding:2px 7px;
    }

    .info-box {
        border:2px solid #333;
        padding:5px;
        font-size:10pt;             /* Font-størrelse for hovedteksten i rapporten*/
        margin-top:10px;
        margin-bottom:15px;
    }

    .subheader {
        background:#99CDFF;
        font-size:16pt;
        text-align:center;
        text-transform:uppercase;
        padding:2px;
        margin:20px 0px 5px 0px;
        font-weight:bold;
    }

    p {font-weight:bold;}

    .red, .yellow, .green, .white, .grey, .orange { 
        text-align:center; 
        width:20px
    }
    .red {
        background-color: red;
    }
    .yellow {
        background-color: yellow;
    }
    .green {
        background-color: green;
    }
    .white {
        background-color: white;
    }
    .grey {
        background-color: #999999;
    }
    .orange {
        background-color: orange;
    }

    .horizontal-header {
        text-align:left;
        vertical-align:bottom;
    }

    .grid {
        border-collapse:collapse;
        border:1px solid #000000;
        color:#000000;
    }

    .header-grid {
        border-collapse:collapse;
        border:none;
        color:#000000;
        width:100%;
    }

    .header-grid th {
        text-align:left;
    }

    .legend-grid {
        border-collapse:collapse;
        border:1px solid #000000;
        width:100%;
        margin-top:20px;
    }

    div.vertical {
        margin-left: -125px;                /* Justerer den vertikale teksten horisontalt */
        position: absolute;
        width: 270px;                       /* Bestemmer høyde på vertikal tekst */ 
        transform: rotate(90deg);
        -webkit-transform: rotate(90deg);   /* Safari/Chrome */
        -moz-transform: rotate(90deg);      /* Firefox */
        -o-transform: rotate(90deg);        /* Opera */
        -ms-transform: rotate(90deg);       /* IE 9 */
    }

    th.vertical {
        height: 270px;                      /* Bestemmer høyde på vertikal tekst */
        line-height: 14px;
        text-align: left;
        /*padding-bottom: 20px;*/
    }
    th {white-space:nowrap}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        
        <!-- Header inkl Logo -->
        <div class="header">
            <table id="HeaderTable" border="0">
                <tr>
                    <td><img src="Images/rapport_logo.jpg" alt="Kystverket" class="logo"  style="resize:both" /></td>
                    <td>
                        <table >
                            <tr><td><h2>Artikler</h2></td></tr>                        
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <asp:GridView ID="ArtiklerGridView" runat="server" AllowPaging="False" AllowSorting="False" CellPadding="4" CellSpacing="1"     Width="100%"
         GridLines="Both" AutoGenerateColumns="false" OnRowDataBound="ArtiklerGridView_RowDataBound" EnableViewState="false" CssClass="grid">
            <Columns>

                <asp:TemplateField HeaderStyle-Width="10%"  HeaderStyle-HorizontalAlign="Left" >
                    <ItemTemplate>
                        <%#Eval("Artikkel")%>
                    </ItemTemplate>
                    <HeaderTemplate>
                        <asp:Label ID="ArtikkelLabel" runat="server" Font-Bold="true" Text="Artikkel" />
                    </HeaderTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderStyle-Width="40%"   HeaderStyle-HorizontalAlign="Left" >
                    <ItemTemplate>
                        <%#Eval("Beskrivelse")%>
                    </ItemTemplate>
                    <HeaderTemplate>
                        <asp:Label ID="BeskrivelseLabel" runat="server" Font-Bold="true" Text="Beskrivelse" />
                    </HeaderTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderStyle-Width="14%"   HeaderStyle-HorizontalAlign="Left" >
                    <ItemTemplate>
                        <%#Eval("Artikkelgruppe")%>
                    </ItemTemplate>
                    <HeaderTemplate>
                        <asp:Label ID="ArtikkelgruppeLabel" runat="server" Font-Bold="true" Text="Artikkelgruppe" />
                    </HeaderTemplate>
                </asp:TemplateField> 
                
                <asp:TemplateField HeaderStyle-Width="10%"   HeaderStyle-HorizontalAlign="Left" >
                    <ItemTemplate>
                        <%#Eval("Enhet")%>
                    </ItemTemplate>
                    <HeaderTemplate>
                        <asp:Label ID="EnhetLabel" runat="server" Font-Bold="true" Text="Enhet" />
                    </HeaderTemplate>
                </asp:TemplateField>   
                
                <asp:TemplateField HeaderStyle-Width="10%"  HeaderStyle-HorizontalAlign="Left"  ItemStyle-ForeColor="DarkBlue"  ItemStyle-HorizontalAlign="Right" >
                    <ItemTemplate   >
                       <%#Eval("Pris")%> 
                    </ItemTemplate>
                    <HeaderTemplate>
                        <asp:Label ID="PrisLabel" runat="server" Font-Bold="true" Text="Pris" />
                    </HeaderTemplate>
                </asp:TemplateField>  
                
                <asp:TemplateField HeaderStyle-Width="8%"  HeaderStyle-HorizontalAlign="Left"   >
                    <ItemTemplate>
                        <%#Eval("Historisk")%>
                    </ItemTemplate>
                    <HeaderTemplate>
                        <asp:Label ID="HistoriskLabel" runat="server" Font-Bold="true" Text="Historisk" />
                    </HeaderTemplate>
                </asp:TemplateField>  

                <asp:TemplateField HeaderStyle-Width="8%"  HeaderStyle-HorizontalAlign="Left"     >
                    <ItemTemplate>
                        <%#Eval("Sperret")%>
                    </ItemTemplate>
                    <HeaderTemplate>
                        <asp:Label ID="SperretLabel" runat="server" Font-Bold="true" Text="Sperret" />
                    </HeaderTemplate>
                </asp:TemplateField>  
                
            </Columns>
        </asp:GridView>

        <!-- FOOTER -->
        <asp:Label ID="DatoHeaderLabel" runat="server" Font-Size="X-Small" />
        
    </form>
</body>
</html>