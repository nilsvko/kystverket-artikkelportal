using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

namespace Artikler
{
    public partial class ArtikkelRapport : System.Web.UI.Page
    {
        #region SQL

        public string listSQL = @"SELECT par_code AS Artikkel, 
		                par_desc AS Beskrivelse, 
		                par_commodity AS Artikkelgruppe, 
		                par_uom AS Enhet, 
		                Cast(Round(ppr_avgprice, 2) AS NUMERIC(24,2)) AS Pris, 
		                CASE IsNull(par_udfchkbox01, '-') WHEN '+' THEN 'Ja' ELSE 'Nei' END AS Historisk,
		                CASE IsNull(par_udfchkbox02, '-') WHEN '+' THEN 'Ja' ELSE 'Nei' END AS Sperret
                        FROM r5parts
                        LEFT OUTER JOIN r5partprices ON ppr_part = par_code AND ppr_part_org = par_org
                        WHERE ppr_part_org = 'MI'
                        AND Upper(par_code) LIKE Upper('%{0}%')
                        AND Upper(par_desc) LIKE Upper('%{1}%')
                        AND par_commodity LIKE '%{2}%'
                        AND Upper(par_uom) LIKE Upper('%{3}%')
                        AND Cast(ppr_avgprice AS NVARCHAR) LIKE '%{4}%'
                        AND par_udfchkbox01 LIKE '%{5}%'
                        AND par_udfchkbox02 LIKE '%{6}%' 
                        ORDER BY {7} ";
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            // NB! Rapportene er sesjonsløse og skal IKKE returnerer til Login-siden hvis sesjonen utløper!           

            string ArtikkelFilter       = (string)Request.QueryString["Artikkel"] ?? "";
            string BeskrivelseFilter    = (string)Request.QueryString["Beskrivelse"] ?? "";
            string ArtikkelgruppeFilter = (string)Request.QueryString["Artikkelgruppe"] ?? "";
            string EnhetFilter          = (string)Request.QueryString["Enhet"] ?? "";
            string PrisFilter           = (string)Request.QueryString["Pris"] ?? "";
            string HistoriskFilter      = (string)Request.QueryString["Historisk"] ?? "";
            string SperretFilter        = (string)Request.QueryString["Sperret"] ?? "";
            string Sortering            = (string)Request.QueryString["SortOrder"] ?? "";

            // Må endre verdier tilbake til "+" hvis "X" i URL
            if (HistoriskFilter == "X") HistoriskFilter = "+";
            if (SperretFilter == "X")   SperretFilter   = "+";

            // Artikler
            ArtiklerGridView.DataSource = ListData(ArtikkelFilter, BeskrivelseFilter, ArtikkelgruppeFilter, EnhetFilter, PrisFilter, HistoriskFilter, SperretFilter, Sortering);
            ArtiklerGridView.DataBind();

            //Footer
            DatoHeaderLabel.Text = "Utskrift av artikkelliste fra Kystverket FFM pr. " + (string)System.DateTime.Now.ToShortDateString();
            DatoHeaderLabel.ForeColor = System.Drawing.Color.DarkRed;
        }

        public DataTable ListData(string ArtikkelFilter, string BeskrivelseFilter, string ArtikkelgruppeFilter, string EnhetFilter, string PrisFilter, string HistoriskFilter, string SperretFilter, string Sortering)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PSWConnectionString"].ConnectionString;   // Fra web.config
            string sql = string.Format(listSQL, ArtikkelFilter, BeskrivelseFilter, ArtikkelgruppeFilter, EnhetFilter, PrisFilter, HistoriskFilter, SperretFilter, Sortering);
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    return dt;
                }
            }
        }

        internal Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }
            foreach (Control c in root.Controls)
            {
            Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }
            return null;
        }

    }
}