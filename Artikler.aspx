﻿<%@ Page Title="Artikler - KV FFM" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Artikler.aspx.cs" Inherits="Artikler._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <asp:Panel ID="Panel1" runat="server"   Height="16px" />
    <asp:Panel ID="HeaderPanel" runat="server"   Height="115px" >

        <!-- Knapperad... -->
        <asp:Panel ID="KnapperadPanel" runat="server" style="text-align: left">   
            <asp:UpdatePanel ID="HeaderUpdatePanel" runat="server" UpdateMode="Always">
                <ContentTemplate>

                    <table>
                        <tr>
                            <td style="width:10px"></td>
                            <td style="width:150px"><asp:Label          ID="HeaderLabel"                    runat="server" Text="Artikler"                  ForeColor="Yellow"  Font-Size="X-Large" /></td>
                            <td style="width:350px"></td>
                        </tr>
                        <tr>
                            <td style="width:10px"></td>
                            <td style="width:150px"></td>
                            <td style="width:350px"></td>                            
                            <td style="width:130px"><asp:Label          ID="AntallPosterLabel"              runat="server" Text="Antall poster"             ForeColor="Yellow"  Style="right:auto;" /></td>
                            <td style="width:180px"><asp:LinkButton     ID="NullstillAlleFilterLinkButton"  runat="server" Text="Nullstill alle filter"     ForeColor="White"   Style="right:auto;"  ToolTip="Nullstiller alle filtre og sorteringer"           OnClick="NullstillAlleFilterLinkButton_Click"/>  </td>
                            <td style="width:40px"></td>
                            <td style="width:20px"><asp:ImageButton     ID="PrintImageButton"               runat="server" ImageUrl="~\Images\Print1.png"   Height="16px"       Style="vertical-align:bottom;" ToolTip="Skriv ut artiklene"    OnClick="PrintImageButton_Click"  OnClientClick="{request = 1;}" /></td>
                            <td style="width:20px"><asp:ImageButton     ID="ExcelImageButton"               runat="server" ImageUrl="~\Images\Excel.jpg"    Height="16px"       Style="vertical-align:bottom;" ToolTip="Eksporter artiklene til Excel"   OnClick="ExcelImageButton_Click"  /></td>
                        </tr>
                        <tr><td /></tr>
                    </table>

                    <br /> 

                </ContentTemplate>
                <Triggers>
                     <asp:PostBackTrigger ControlID="ExcelImageButton" />
                </Triggers> 
            </asp:UpdatePanel>

        </asp:Panel>    <!-- KnapperadPanel> 

       <!-- Listevisning Artikler -->
       <!-- HEADER -->
        <asp:UpdatePanel ID="GridViewHeaderUpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>

                <div style="HEIGHT:50px">

                        <asp:GridView ID="GridViewHeader" runat="server" 
                         
                                        Showheader              =   "true" 
                                        ShowHeaderWhenEmpty     =   "true"
                                        ShowFooter              =   "false"
                                        AutoGenerateColumns     =   "false"  
                                        AutoGenerateSelectButton=   "false"
                                        Width                   =   "900px" 
                                        EditRowStyle-Height     =   "1px"
                                        AllowSorting            =   "true" 
                                        CellPadding             =   "4" 
                                        CellSpacing             =   "1" 
                                        AllowPaging             =   "true"
                                        PageSize                =   "1"
                                        PagerSettings-Visible   =   "false" 
                                        OnSorting               =   "GridViewHeader_Sorting"    
                        >
                            <Columns>
                              
                                 <asp:TemplateField SortExpression="Artikkel"                           HeaderStyle-Width="10%">
                                    <HeaderTemplate>
                                        <asp:LinkButton     ID="ArtikkelLinkButton"                     runat="server"      Text="Artikkel"         AutoPostBack="true" CommandName="Sort"  CommandArgument="Artikkel"       TabIndex="11" /><br />                                                    
                                        <asp:TextBox        ID="ArtikkelFilterTextBox"                  runat="server"      Width="60px"            AutoPostBack="true" ForeColor="Black"  OnTextChanged="ArtikkelFilterTextBox_TextChanged" />
                                    </HeaderTemplate>
                                </asp:TemplateField> 

                                <asp:TemplateField SortExpression="Beskrivelse"                         HeaderStyle-Width="40%">
                                    <HeaderTemplate>
                                        <asp:LinkButton     ID="BeskrivelseLinkButton"                  runat="server"      Text="Beskrivelse"      CommandName="Sort"  CommandArgument="Beskrivelse"    TabIndex="12" /><br />  
                                        <asp:TextBox        ID="BeskrivelseFilterTextBox"               runat="server"      Width="300px"           AutoPostBack="true" ForeColor="Black"  OnTextChanged="BeskrivelseFilterTextBox_TextChanged"/>
                                    </HeaderTemplate>
                                </asp:TemplateField> 

                                <asp:TemplateField SortExpression="Artikkelgruppe"                      HeaderStyle-Width="14%">
                                    <HeaderTemplate>
                                        <asp:LinkButton     ID="ArtikkelgruppeLinkButton"               runat="server"      Text="Artikkelgruppe"   CommandName="Sort"  CommandArgument="Artikkelgruppe" TabIndex="13" /><br />   
                                        <asp:DropDownList   ID="ArtikkelgruppeFilterDropDownList"       runat="server"      Width="80px"            AutoPostBack="true" ForeColor="Black"   OnSelectedIndexChanged="ArtikkelgruppeFilterDropDownList_SelectedIndexChanged" />
                                    </HeaderTemplate>
                                </asp:TemplateField> 

                                <asp:TemplateField SortExpression="Enhet"                               HeaderStyle-Width="10%">
                                    <HeaderTemplate>
                                        <asp:LinkButton     ID="EnhetLinkButton"                        runat="server"      Text="Enhet"            CommandName="Sort"  CommandArgument="Enhet"          TabIndex="14" /><br />                           
                                        <asp:TextBox        ID="EnhetFilterTextBox"                     runat="server"      Width="60px"            AutoPostBack="true" ForeColor="Black"   OnTextChanged="EnhetFilterTextBox_TextChanged"/>
                                    </HeaderTemplate>
                                </asp:TemplateField> 

                                <asp:TemplateField SortExpression="Pris"                                HeaderStyle-Width="10%">
                                    <HeaderTemplate>
                                        <asp:LinkButton     ID="PrisLinkButton"                         runat="server"      Text="Pris"             CommandName="Sort" CommandArgument="Pris"           TabIndex="15" /><br />   
                                        <asp:TextBox        ID="PrisFilterTextBox"                      runat="server"      Width="60px"            AutoPostBack="true"   ForeColor="Black"  OnTextChanged="PrisFilterTextBox_TextChanged"/>
                                    </HeaderTemplate>
                                </asp:TemplateField> 

                                <asp:TemplateField SortExpression="Historisk"                           HeaderStyle-Width="8%">
                                    <HeaderTemplate>
                                        <asp:LinkButton     ID="HistoriskLinkButton"                    runat="server"     Text="Historisk"         CommandName="Sort" CommandArgument="Historisk"      TabIndex="16" /><br />   
                                        <asp:DropDownList   ID="HistoriskFilterDropDownList"            runat="server"     Width="48px"             AutoPostBack="true" ForeColor="Black"   OnSelectedIndexChanged="HistoriskFilterDropDownList_SelectedIndexChanged"/>
                                    </HeaderTemplate>
                                </asp:TemplateField> 

                                <asp:TemplateField SortExpression="Sperret"                             HeaderStyle-Width="8%">
                                    <HeaderTemplate>
                                        <asp:LinkButton     ID="SperretLinkButton"                      runat="server"    Text="Sperret"            CommandName="Sort" CommandArgument="Sperret"         TabIndex="17" /><br />  
                                        <asp:DropDownList   ID="SperretFilterDropDownList"              runat="server"    Width="48px"              AutoPostBack="true" ForeColor="Black"   OnSelectedIndexChanged="SperretFilterDropDownList_SelectedIndexChanged"/>
                                    </HeaderTemplate>
                                </asp:TemplateField> 

                            </Columns>

                            <HeaderStyle CssClass="EAMHeader" />
                            <PagerStyle CssClass="EAMPager" HorizontalAlign="Right" />            
                            <RowStyle CssClass="EAMRow" />
                            <SelectedRowStyle CssClass="EAMSelectedRow" />            
                            <AlternatingRowStyle CssClass="EAMAltRow" />

                        </asp:GridView>
                    <!-- Slutt HEADER -->

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </asp:Panel>

    <asp:Panel ID="ArtiklerPanel" runat="server" Height="900px" Width="925px" ScrollBars="Vertical" >

        <asp:UpdatePanel ID="GridViewUpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div>
            
                    <!-- LISTE -->
                    <asp:GridView ID="GridViewList" runat="server" 
                        ShowHeaderWhenEmpty="true" 
                        AutoGenerateSelectButton="True" 
                        AutoGenerateColumns ="false"  
                        Width="900px"  
                        EditRowStyle-Height="20px"  
                        ShowHeader="false"
                        OnRowDataBound="GridViewList_RowDataBound" 
                        OnSelectedIndexChanged="GridViewList_SelectedIndexChanged"
                        AllowPaging="False"  
                        AllowSorting="True" 
                        OnSorting="GridViewList_Sorting"
                    >
                        <Columns>

                                <asp:BoundField DataField="Artikkel"                ItemStyle-Width="10%"       SortExpression="Artikkel" />
                                <asp:BoundField DataField="Beskrivelse"             ItemStyle-Width="40%"       SortExpression="Beskrivelse"        HtmlEncode="false" />
                                <asp:BoundField DataField="Artikkelgruppe"          ItemStyle-Width="14%"       SortExpression="Artikkelgruppe"     HtmlEncode="false" />
                                <asp:BoundField DataField="Enhet"                   ItemStyle-Width="10%"       SortExpression="Enhet"              HtmlEncode="false" />
                                <asp:BoundField DataField="Pris"                    ItemStyle-Width="10%"       SortExpression="Pris"               ItemStyle-HorizontalAlign="Right" ItemStyle-ForeColor="DarkBlue"/>
                                <asp:BoundField DataField="Historisk"               ItemStyle-Width="8%"        SortExpression="Historisk"          />
                                <asp:BoundField DataField="Sperret"                 ItemStyle-Width="8%"        SortExpression="Sperret"            />

                        </Columns>

                        <emptydatarowstyle CssClass="emptydatarow" /><emptydatatemplate><asp:image id="NoRecordsFound" ImageAlign="AbsMiddle"
                            imageurl="~/Images/objects_24.png"
                            alternatetext="No Records Found" 
                            runat="server" CssClass="emptydatarow-img" />Ingen treff i databasen. Gå tilbake for å <a href="javascript:history.back();">søke på nytt</a>.
                        </emptydatatemplate><HeaderStyle CssClass="EAMHeader" />
                        <PagerStyle CssClass="EAMPager" HorizontalAlign="Right" />            
                        <RowStyle CssClass="EAMRow" />
                        <SelectedRowStyle CssClass="EAMSelectedRow" />            
                        <AlternatingRowStyle CssClass="EAMAltRow" />

                    </asp:GridView>
                    <!-- Slutt ITEMS -->
            
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </asp:Panel> <!-- ArtiklerPanel -->


</asp:Content>
