﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Artikler
{
    public partial class _Default : Page
    {
        #region Deklarasjoner
        // Sortering
        public string SortDirection
        {
            get
            {
                if (ViewState["SortDirection"] == null)
                    return string.Empty;
                else
                    return ViewState["SortDirection"].ToString();
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }

        private string _sortDirection;
        Image sortImage = new Image();
        #endregion

        #region SQL
        string Asql = @"SELECT par_code AS Artikkel, 
		                par_desc AS Beskrivelse, 
		                par_commodity AS Artikkelgruppe, 
		                par_uom AS Enhet, 
		                Cast(Round(ppr_avgprice, 2) AS NUMERIC(24,2)) AS Pris, 
		                CASE IsNull(par_udfchkbox01, '-') WHEN '+' THEN 'Ja' ELSE 'Nei' END AS Historisk,
		                CASE IsNull(par_udfchkbox02, '-') WHEN '+' THEN 'Ja' ELSE 'Nei' END AS Sperret
                        FROM r5parts
                        LEFT OUTER JOIN r5partprices ON ppr_part = par_code AND ppr_part_org = par_org
                        WHERE ppr_part_org = 'MI'
                        AND Upper(par_code) LIKE Upper('%{0}%')
                        AND Upper(par_desc) LIKE Upper('%{1}%')
                        AND par_commodity LIKE '%{2}%'
                        AND Upper(par_uom) LIKE Upper('%{3}%')
                        AND Cast(ppr_avgprice AS NVARCHAR) LIKE '%{4}%'
                        AND par_udfchkbox01 LIKE '%{5}%'
                        AND par_udfchkbox02 LIKE '%{6}%' 
                        ORDER BY {7} ";

        string cSql = "SELECT cmd_code as Code FROM r5commodities";
        #endregion

        #region Oppstart
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["EASortParam"] == null) Session["EASortParam"] = "Artikkel";
                if (Session["EASortDir"] == null) SetSortDirection("");
                SettHeader();
                SettOppslagverdier();
                SettListevisning();
            }
        }
        #endregion

        #region Oppslagsverdier
        private void SettOppslagverdier()
        {
            DropDownList gDropDownList = FindControlRecursive(Page, "ArtikkelgruppeFilterDropDownList") as DropDownList;
            gDropDownList.Items.Clear();
            gDropDownList.DataTextField = "Code";
            gDropDownList.DataValueField = "Code";
            gDropDownList.DataSource = LastInnArtikkelgrupper();
            gDropDownList.DataBind();
            gDropDownList.Items.Insert(0, new ListItem("Alle", ""));

            // Historisk
            DropDownList hDropDownList = FindControlRecursive(Page, "HistoriskFilterDropDownList") as DropDownList;
            hDropDownList.Items.Add(new ListItem("Alle", "%"));
            hDropDownList.Items.Add(new ListItem("Nei", "-"));
            hDropDownList.Items.Add(new ListItem("Ja", "+"));

            // Sperret
            DropDownList sDropDownList = FindControlRecursive(Page, "SperretFilterDropDownList") as DropDownList;
            sDropDownList.Items.Add(new ListItem("Alle", "%"));
            sDropDownList.Items.Add(new ListItem("Nei", "-"));
            sDropDownList.Items.Add(new ListItem("Ja", "+"));

        }
        protected DataTable LastInnArtikkelgrupper()
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PSWConnectionString"].ConnectionString;   // Fra web.config

            string sql = cSql;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    return dt;
                }
            }
        }
        #endregion

        #region Gridview
        public void SettListevisning()
        {

            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PSWConnectionString"].ConnectionString;   // Fra web.config
            DataTable dt = new DataTable();

            TextBox aTextBox = FindControlRecursive(Page, "ArtikkelFilterTextBox") as TextBox;
            string strArtikkel = aTextBox.Text ?? "";
            TextBox bTextBox = FindControlRecursive(Page, "BeskrivelseFilterTextBox") as TextBox;
            string strBeskrivelse = bTextBox.Text ?? "";
            DropDownList gDropDownList = FindControlRecursive(Page, "ArtikkelgruppeFilterDropDownList") as DropDownList;
            string strArtikkelgruppe = gDropDownList.SelectedValue;
            TextBox eTextBox = FindControlRecursive(Page, "EnhetFilterTextBox") as TextBox;
            string strEnhet = eTextBox.Text ?? "";
            TextBox pTextBox = FindControlRecursive(Page, "PrisFilterTextBox") as TextBox;
            string strPris = pTextBox.Text ?? "";
            DropDownList hDropDownList = FindControlRecursive(Page, "HistoriskFilterDropDownList") as DropDownList;
            string strHistorisk = hDropDownList.SelectedValue;
            DropDownList sDropDownList = FindControlRecursive(Page, "SperretFilterDropDownList") as DropDownList;
            string strSperret = sDropDownList.SelectedValue;

            string sortParam = Session["EASortParam"].ToString() + " " + Session["EASortDir"].ToString();

            string sql = String.Format(Asql, strArtikkel, strBeskrivelse, strArtikkelgruppe, strEnhet, strPris, strHistorisk, strSperret, sortParam);

            // Export til Excel parametre
            Session["ExcelSQLQuery"] = sql;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }

                GridViewList.DataSource = dt;
                GridViewList.DataBind();
                int intAntallPoster = GridViewList.Rows.Count;
                AntallPosterLabel.Text = "Antall poster: " + intAntallPoster.ToString();
            }

            settPiler();

        }

        public void SettHeader()
        {

            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PSWConnectionString"].ConnectionString;   // Fra web.config
            DataTable dt = new DataTable();

            string sql = String.Format(Asql, "XXXXXzXXXXX", "", "", "", "", "", "", "Artikkel DESC");

            // Export til Excel parametre
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                conn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }

                GridViewHeader.DataSource = dt;
                GridViewHeader.DataBind();
            }
        }
        protected void GridViewList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[0].Style["display"] = "none";

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#93A3B0'; this.style.color='White'; this.style.cursor='pointer'");
                e.Row.Attributes.Add("onmouseout", String.Format("this.style.color='Black';this.style.backgroundColor='white';"));
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridViewList, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Klikk for å velge denne raden";
            }
        }

        protected void GridViewList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridViewList_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        #endregion

        #region Sortering
        // Sorter  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        // Sorteringsrekkefølge
        protected void SetSortDirection(string sortDirection)
        {
            if (sortDirection == "ASC")
            {
                _sortDirection = "DESC";
                sortImage.ImageUrl = "./images/view_sort_descending.png";

            }
            else
            {
                _sortDirection = "ASC";
                sortImage.ImageUrl = "./images/view_sort_ascending1.png";
            }

            Session["EASortDir"] = _sortDirection;
        }

        // Sortering av listevisning
        protected void GridViewHeader_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression.ToString();
            string orgSortExpression = Session["EASortParam"].ToString();
            Session["EASortParam"] = sortExpression;

            if (sortExpression == orgSortExpression)
            {
                SortDirection = Session["EASortDir"].ToString();
                SetSortDirection(SortDirection);
            }
            else
            {
                _sortDirection = "ASC";
            }

            SortDirection = _sortDirection;
            Session["EASortDir"] = SortDirection;

            SettListevisning();
        }

        // Setter sorteringspiler
        private void settPiler()
        {
            // henter retningspil
            string sortExpression = Session["EASortParam"].ToString();
            string sortDirection = Session["EASortDir"].ToString();

            if (sortDirection == "DESC")
            {
                sortImage.ImageUrl = "./images/view_sort_descending.png";
                sortImage.ToolTip = "Synkende";
            }
            else
            {
                sortImage.ImageUrl = "./images/view_sort_ascending1.png";
                sortImage.ToolTip = "Stigende";
            }

            //finner kolonne
            int columnIndex = 0;
            foreach (DataControlFieldHeaderCell headerCell in GridViewHeader.HeaderRow.Cells)
            {
                if (headerCell.ContainingField.SortExpression == sortExpression)
                {
                    columnIndex = GridViewHeader.HeaderRow.Cells.GetCellIndex(headerCell);
                }
            }

            GridViewHeader.HeaderRow.Cells[columnIndex].Controls.Add(sortImage);

        }
        #endregion

        #region Søk
        protected void NullstillAlleFilterLinkButton_Click(object sender, EventArgs e)
        {
            TextBox aTextBox = FindControlRecursive(Page, "ArtikkelFilterTextBox") as TextBox;
            aTextBox.Text = "";
            TextBox bTextBox = FindControlRecursive(Page, "BeskrivelseFilterTextBox") as TextBox;
            bTextBox.Text = "";
            DropDownList gDropDownList = FindControlRecursive(Page, "ArtikkelgruppeFilterDropDownList") as DropDownList;
            gDropDownList.SelectedIndex = -1;
            TextBox eTextBox = FindControlRecursive(Page, "EnhetFilterTextBox") as TextBox;
            eTextBox.Text = "";
            TextBox pTextBox = FindControlRecursive(Page, "PrisFilterTextBox") as TextBox;
            pTextBox.Text = "";
            DropDownList hDropDownList = FindControlRecursive(Page, "HistoriskFilterDropDownList") as DropDownList;
            hDropDownList.SelectedIndex = -1;
            DropDownList sDropDownList = FindControlRecursive(Page, "SperretFilterDropDownList") as DropDownList;
            sDropDownList.SelectedIndex = -1;

            Session["EASortDir"] = "ASC";
            Session["EASortParam"] = "Artikkel";

            SettListevisning();
        }


        protected void ArtikkelFilterTextBox_TextChanged(object sender, EventArgs e)
        {
            SettListevisning();
        }

        protected void BeskrivelseFilterTextBox_TextChanged(object sender, EventArgs e)
        {
            SettListevisning();
        }

        protected void ArtikkelgruppeFilterDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SettListevisning();
        }

        protected void EnhetFilterTextBox_TextChanged(object sender, EventArgs e)
        {
            SettListevisning();
        }

        protected void PrisFilterTextBox_TextChanged(object sender, EventArgs e)
        {
            SettListevisning();
        }

        protected void HistoriskFilterDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SettListevisning();
        }

        protected void SperretFilterDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SettListevisning();
        }

        #endregion

        #region Utskrift
        protected void PrintImageButton_Click(object sender, ImageClickEventArgs e)
        {
            TextBox aTextBox            = FindControlRecursive(Page, "ArtikkelFilterTextBox") as TextBox;
            string strArtikkel          = aTextBox.Text ?? "";
            TextBox bTextBox            = FindControlRecursive(Page, "BeskrivelseFilterTextBox") as TextBox;
            string strBeskrivelse       = bTextBox.Text ?? "";
            DropDownList gDropDownList  = FindControlRecursive(Page, "ArtikkelgruppeFilterDropDownList") as DropDownList;
            string strArtikkelgruppe    = gDropDownList.SelectedValue;
            TextBox eTextBox            = FindControlRecursive(Page, "EnhetFilterTextBox") as TextBox;
            string strEnhet             = eTextBox.Text ?? "";
            TextBox pTextBox            = FindControlRecursive(Page, "PrisFilterTextBox") as TextBox;
            string strPris              = pTextBox.Text ?? "";
            DropDownList hDropDownList  = FindControlRecursive(Page, "HistoriskFilterDropDownList") as DropDownList;
            string strHistorisk         = hDropDownList.SelectedValue;        
            DropDownList sDropDownList  = FindControlRecursive(Page, "SperretFilterDropDownList") as DropDownList;
            string strSperret           = sDropDownList.SelectedValue;

            // Må endre verdier hvis "+" før de sendes i URL
            if (strHistorisk == "+")    strHistorisk    = "X";
            if (strSperret == "+")      strSperret      = "X";

            string sortParam            = Session["EASortParam"].ToString() + " " + Session["EASortDir"].ToString();
            string strRapportLink       = "RunAsPDF.ashx?RapportNavn=ArtikkelRapport&Artikkel=" + strArtikkel + "&Beskrivelse=" + strBeskrivelse + "&Artikkelgruppe=" + strArtikkelgruppe + "&Enhet=" + strEnhet + "&Pris=" + strPris + "&Historisk=" + strHistorisk + "&Sperret=" + strSperret + "&SortOrder=" + sortParam;
          
            Response.Redirect(strRapportLink, true);
        }
        #endregion

        #region ExportTilExcel
        protected void ExcelImageButton_Click(object sender, ImageClickEventArgs e)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PSWConnectionString"].ConnectionString;   // Fra web.config
            string defaultSQL = (string)Session["ExcelSQLQuery"] ?? "";
            string FileName = "Artikler"; 
            string ServerLocalPath = Server.MapPath("~/");

            if (defaultSQL != "" && FileName != "")
            {
                ExportToExcel excelExport = new ExportToExcel();
                string retur = excelExport.ExportFile(FileName, ServerLocalPath , defaultSQL, connectionString);
            }
        }
        #endregion

        #region Diverse
        protected Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }
            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }
            return null;
        }
        #endregion

    }





}