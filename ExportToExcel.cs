﻿
using System;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using ClosedXML.Excel;      // This package need sto be imported from NuGet

namespace Artikler
{
    public class ExportToExcel
    {

            string ReturnMessage = "OK";
            public string ExportFile(string strFileName, string ServerPath, string strSQLStatement, string connectionString)
            {
                if (strFileName != "" && ServerPath != "" && strSQLStatement != "" && connectionString != "")
                {
                    // Oppretter datatable
                    DataTable dt = new DataTable();

                    using (SqlConnection conn = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd = new SqlCommand(strSQLStatement, conn);

                        conn.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            dt.Load(reader);
                        }
                    }

                    // Oppretter Excel fil
                    XLWorkbook wb = new XLWorkbook();
                    wb.Worksheets.Add(dt, strFileName);
                    wb.Author = "Prevas AS";
                    string pathServer = ServerPath + "Export\\";

                    //oppretter filnavn
                    string strTimeStamp = System.DateTime.Now.Year.ToString() + "-" + String.Format("{0:00}", System.DateTime.Now.Month) + "-" + String.Format("{0:00}", System.DateTime.Now.Day);
                    string ExcelFileName = strFileName + "_" + strTimeStamp + ".xlsx";

                    // Sletter temp-fil på server hvis forrige cleanup gikk feil
                    File.Delete(pathServer + ExcelFileName);

                    // Oppretter selve filen
                    FileStream wbFileStream = new FileStream(pathServer + ExcelFileName, FileMode.CreateNew);
                    wb.SaveAs(wbFileStream);
                    wb.Dispose();
                    wbFileStream.Dispose();

                    // Lar bruker laste ned fil
                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    string transferFile = pathServer + ExcelFileName;
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "Application/Excel";
                    response.AddHeader("Content-Disposition", "attachment; filename=" + ExcelFileName + ";");
                    response.TransmitFile(transferFile);
                    response.Flush();

                    // Sletter temp-fil på server
                    File.Delete(pathServer + ExcelFileName);

                    response.End();
                }
                else
                {
                    ReturnMessage = " All input parameters must have a value!";
                }

                return ReturnMessage;

            }
        }
   
}