using System;
using System.IO;
using System.Web;
using ExpertPdf.HtmlToPdf;
using iTextSharp.text.pdf;

namespace Artikler
{
    /// <summary>
    /// Summary description for RunAsPDF1
    /// </summary>
    public class RunAsPDF : IHttpHandler
    {
        private string strRapportNavn       = default(string);
        private string strArtikkel          = default(string);
        private string strBeskrivelse       = default(string);
        private string strArtikkelgruppe    = default(string);
        private string strEnhet             = default(string);
        private string strPris              = default(string);
        private string strHistorisk         = default(string);
        private string strSperret           = default(string);
        private string strSortering         = default(string);
        private string baseUrl              = default(string);
        private string urlToConvert         = default(string);
        private string headerUrl            = default(string);
        private string footerUrl            = default(string);
        private string pageOrientation      = default(string);
        bool relativeUrls                   = false;

        public void ProcessRequest(HttpContext context)
        {
            // Base url
            baseUrl = $"{context.Request.Url.Scheme}://{context.Request.Url.Authority}{context.Request.ApplicationPath.TrimEnd('/')}/"; // Always end the string with "/"

            // Url to convert: start with base
            urlToConvert = baseUrl;

            // Input parameters
            strRapportNavn      = context.Request.QueryString["RapportNavn"] ?? "";
            strArtikkel         = context.Request.QueryString["Artikkel"] ?? "";
            strBeskrivelse      = context.Request.QueryString["Beskrivelse"] ?? "";
            strArtikkelgruppe   = context.Request.QueryString["Artikkelgruppe"] ?? "";
            strEnhet            = context.Request.QueryString["Enhet"] ?? "";
            strPris             = context.Request.QueryString["Pris"] ?? "";
            strHistorisk        = context.Request.QueryString["Historisk"] ?? "";
            strSperret          = context.Request.QueryString["Sperret"] ?? "";
            strSortering        = context.Request.QueryString["SortOrder"] ?? "";

            // Report settings
            strRapportNavn = strRapportNavn.ToUpper();
    
            switch (strRapportNavn)
            {
                case "ARTIKKELRAPPORT":
                    pageOrientation = PDFPageOrientation.Portrait.ToString();
                    urlToConvert += $"ArtikkelRapport.aspx?Artikkel={strArtikkel}&Beskrivelse={strBeskrivelse}&Artikkelgruppe={strArtikkelgruppe}&Enhet={strEnhet}&Pris={strPris}&Historisk={strHistorisk}&Sperret={strSperret}&SortOrder={strSortering}";
                    break;

                default:
                    // No default report to run
                    urlToConvert = string.Empty;
                    break;
            }

            if (!string.IsNullOrEmpty(urlToConvert))
            {
                // Run report
                RunReport(strRapportNavn, urlToConvert, pageOrientation, context);
            }
            else
            {
                // Error 
                context.Response.ContentType = "text/plain";
                context.Response.Write("Error: Invalid input parameters");
            }
        }

        private void RunReport(string reportName, string urlToConvert, string pageOrientation, HttpContext context)
        {
            string strTimeStamp = System.DateTime.Now.Year.ToString() + "-" + String.Format("{0:00}", System.DateTime.Now.Month) + "-" + String.Format("{0:00}", System.DateTime.Now.Day);
            string downloadName = "Artikler_" + strTimeStamp + ".pdf";
            byte[] downloadBytes = null;

            PdfConverter pdfConverter = GetPdfConverter(reportName);

            if (!relativeUrls)
            {
                downloadBytes = pdfConverter.GetPdfBytesFromUrl(urlToConvert);
            }
            else
            {
                PdfReader reader = new PdfReader(pdfConverter.GetPdfBytesFromUrl(urlToConvert));
                downloadBytes = UpdateLinks(reader);
            }

            context.Response.Clear();
            //context.Response.AddHeader("Content-Type", "application/pdf");
            context.Response.AddHeader("Content-Type", "binary/octet-stream");
            context.Response.AddHeader("Content-Disposition", $"attachment; filename={downloadName}; size={downloadBytes.Length.ToString()}");
            context.Response.Flush();
            context.Response.BinaryWrite(downloadBytes);
            context.Response.Flush();
            context.Response.End();
        }

        private PdfConverter GetPdfConverter(string reportName)
        {
            PdfConverter pdfConverter = new PdfConverter();

            // Prevas AS Lisensnøkkel
            pdfConverter.LicenseKey = "ORILGQEZDQgIGQAXCRkKCBcICxcAAAAA";

            // Set the PDF page orientation (portrait or landscape)
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = (PDFPageOrientation)Enum.Parse(typeof(PDFPageOrientation), pageOrientation);

            // Set if the PDF page should be automatically resized to the size 
            // of the HTML content when FitWidth is false (default)
            pdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;

            //Header og Footer 
            if (reportName == "OVERSIKT" || reportName == "TOTAL")
            {
                // Egne marginer for Oversiktsrapport
                pdfConverter.PdfDocumentOptions.LeftMargin = 10;
                pdfConverter.PdfDocumentOptions.RightMargin = 10;
                AddHeader(pdfConverter);
                AddFooter(pdfConverter);
            }

            return pdfConverter;
        }

        private byte[] UpdateLinks(PdfReader reader)
        {
            // Setup some variables to be used later
            PdfDictionary pageDictionary = default(PdfDictionary);
            PdfArray annotations = default(PdfArray);

            // Loop through each page
            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
                // Get the current page
                pageDictionary = reader.GetPageN(i);

                // Get all of the annotations for the current page
                annotations = pageDictionary.GetAsArray(PdfName.ANNOTS);

                // Make sure we have something
                if (annotations == null || annotations.Length == 0)
                    continue;

                foreach (PdfObject A in annotations.ArrayList)
                {
                    // Convert the itext-specific object as a generic PDF object
                    PdfDictionary annotationDictionary = (PdfDictionary)PdfReader.GetPdfObject(A);

                    // Make sure this annotation has a link
                    if (!annotationDictionary.Get(PdfName.SUBTYPE).Equals(PdfName.LINK))
                        continue;

                    // Make sure this annotation has an ACTION
                    if (annotationDictionary.Get(PdfName.A) == null)
                        continue;

                    // Get the ACTION for the current annotation
                    PdfDictionary annotationAction = (PdfDictionary)annotationDictionary.Get(PdfName.A);

                    // Test if it is a URI action
                    if (annotationAction.Get(PdfName.S).Equals(PdfName.URI))
                    {
                        // Get existing link
                        string url = annotationAction.Get(PdfName.URI).ToString();

                        // Change the URI to something else
                        annotationAction.Put(PdfName.URI, new PdfString(url.Replace(baseUrl, string.Empty)));
                    }
                }
            }

            // Get and return byte array
            using (MemoryStream ms = new MemoryStream())
            {
                using (PdfStamper stamper = new PdfStamper(reader, ms))
                {
                    stamper.FormFlattening = true;
                }

                reader.Close();

                return ms.ToArray();
            }
        }

        #region Header & Footer
        private void AddHeader(PdfConverter pdfConverter)
        {
            // Enable header
            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            
            // Set the header height in points
            pdfConverter.PdfHeaderOptions.HeaderHeight = 70;
            
            // Set the header HTML area
            pdfConverter.PdfHeaderOptions.HtmlToPdfArea = new HtmlToPdfArea(headerUrl);

            // Page numbers
            pdfConverter.PdfHeaderOptions.TextArea = new TextArea(499, 55, 70, "Side &p; av &P; ",
                new System.Drawing.Font(new System.Drawing.FontFamily("Helvetica"), 8, System.Drawing.GraphicsUnit.Point));
            pdfConverter.PdfHeaderOptions.TextArea.ForeColor = System.Drawing.Color.DarkGray;
            pdfConverter.PdfHeaderOptions.TextArea.TextAlign = HorizontalTextAlign.Right;
        }


        // Footer for dokumentet
        private void AddFooter(PdfConverter pdfConverter)
        {
            // Enable footer
            pdfConverter.PdfDocumentOptions.ShowFooter = true;

            // Set the header HTML area
            pdfConverter.PdfFooterOptions.HtmlToPdfArea = new HtmlToPdfArea(footerUrl);
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}